package services;

import javafx.util.Pair;
import model.Request;
import model.Resource;
import org.junit.Test;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class ResourceServiceTest {

    private ResourceService resourceService = new ResourceService();

    @Test
    public void shouldGetResources() {
        List<Request> requests = generatRequestSampleList();
        List<Resource> result = resourceService.getResources(requests, 1);
        Resource expected = Resource.builder().resource("123").avgDuration(7).build();

        assertEquals(1, result.size());
        assertEquals(expected.getResource(), result.get(0).getResource());
        assertEquals(expected.getAvgDuration(), result.get(0).getAvgDuration(), 0.0);
    }

    @Test
    public void shouldGetResourcesWithNonIntegerAverage() {
        List<Request> requests = generatRequestSampleListWithFractions();
        List<Resource> result = resourceService.getResources(requests, 1);
        Resource expected = Resource.builder().resource("120").avgDuration(5.25).build();

        assertEquals(1, result.size());
        assertEquals(expected.getResource(), result.get(0).getResource());
        assertEquals(expected.getAvgDuration(), result.get(0).getAvgDuration(), 0.0);
    }

    @Test
    public void resourcesShouldBeSorted() {
        List<Request> requests = generatRequestSampleListWithTime();
        List<Resource> expected = generatResourceSampleList();
        List<Resource> result = resourceService.getResources(requests, 3);

        assertEquals(3, result.size());
        assertEquals(expected.get(0).getResource(), result.get(0).getResource());
        assertEquals(expected.get(1).getResource(), result.get(1).getResource());
        assertEquals(expected.get(2).getResource(), result.get(2).getResource());
    }

    @Test
    public void shouldGetHourlyRequests() {
        List<Request> requests = generatRequestSampleListWithTime();
        List<Resource> resources = resourceService.getResources(requests, 3);
        List<Pair<LocalDateTime, Integer>> result = resourceService.getHourlyRequests(requests, resources);

        assertEquals(4, result.size());
        assertEquals(new Pair<>(LocalDateTime.of(2018, 11, 5, 1, 0), 1), result.get(0));
        assertEquals(new Pair<>(LocalDateTime.of(2018, 11, 5, 2, 0), 1), result.get(1));
        assertEquals(new Pair<>(LocalDateTime.of(2018, 11, 5, 3, 0), 2), result.get(2));
        assertEquals(new Pair<>(LocalDateTime.of(2018, 11, 5, 4, 0), 2), result.get(3));
    }

    @Test
    public void shouldKeepEmptyHours() {
        List<Request> requests = generatRequestSampleListWithTime();
        List<Resource> resources = resourceService.getResources(requests, 2);
        List<Pair<LocalDateTime, Integer>> result = resourceService.getHourlyRequests(requests, resources);

        assertEquals(4, result.size());
        assertEquals(new Pair<>(LocalDateTime.of(2018, 11, 5, 1, 0), 0), result.get(0));
        assertEquals(new Pair<>(LocalDateTime.of(2018, 11, 5, 2, 0), 0), result.get(1));
        assertEquals(new Pair<>(LocalDateTime.of(2018, 11, 5, 3, 0), 2), result.get(2));
        assertEquals(new Pair<>(LocalDateTime.of(2018, 11, 5, 4, 0), 1), result.get(3));
    }


    private List<Request> generatRequestSampleList() {
        List<Request> requests = new ArrayList<>();
        requests.add(Request.builder().resource("120").duration(2).build());
        requests.add(Request.builder().resource("121").duration(4).build());
        requests.add(Request.builder().resource("122").duration(2).build());
        requests.add(Request.builder().resource("120").duration(4).build());
        requests.add(Request.builder().resource("122").duration(6).build());
        requests.add(Request.builder().resource("123").duration(6).build());
        requests.add(Request.builder().resource("123").duration(8).build());
        return requests;
    }

    private List<Request> generatRequestSampleListWithFractions() {
        List<Request> requests = new ArrayList<>();
        requests.add(Request.builder().resource("120").duration(5).build());
        requests.add(Request.builder().resource("121").duration(4).build());
        requests.add(Request.builder().resource("120").duration(5).build());
        requests.add(Request.builder().resource("120").duration(5).build());
        requests.add(Request.builder().resource("120").duration(6).build());

        return requests;
    }

    private List<Request> generatRequestSampleListWithTime() {
        List<Request> requests = new ArrayList<>();
        requests.add(Request.builder().time(LocalDateTime.of(2018, 11, 5, 1, 0)).resource("120").duration(2).build());
        requests.add(Request.builder().time(LocalDateTime.of(2018, 11, 5, 1, 0)).resource("121").duration(4).build());
        requests.add(Request.builder().time(LocalDateTime.of(2018, 11, 5, 1, 0)).resource("122").duration(2).build());
        requests.add(Request.builder().time(LocalDateTime.of(2018, 11, 5, 2, 0)).resource("120").duration(4).build());
        requests.add(Request.builder().time(LocalDateTime.of(2018, 11, 5, 2, 0)).resource("121").duration(2).build());
        requests.add(Request.builder().time(LocalDateTime.of(2018, 11, 5, 3, 0)).resource("123").duration(6).build());
        requests.add(Request.builder().time(LocalDateTime.of(2018, 11, 5, 3, 0)).resource("124").duration(10).build());
        requests.add(Request.builder().time(LocalDateTime.of(2018, 11, 5, 4, 0)).resource("120").duration(6).build());
        requests.add(Request.builder().time(LocalDateTime.of(2018, 11, 5, 4, 0)).resource("123").duration(8).build());
        return requests;
    }

    private List<Resource> generatResourceSampleList() {
        List<Resource> resources = new ArrayList<>();
        resources.add(Resource.builder().resource("124").avgDuration(10).build());
        resources.add(Resource.builder().resource("123").avgDuration(7).build());
        resources.add(Resource.builder().resource("120").avgDuration(4).build());
        return resources;
    }
}