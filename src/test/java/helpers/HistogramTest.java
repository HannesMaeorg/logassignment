package helpers;

import javafx.util.Pair;
import org.junit.Test;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class HistogramTest {

    @Test
    public void shouldGenerateBasicHistogram() {
        List<Pair<LocalDateTime, Integer>> pairList = generateBasicPairList();
        List<String> result = Histogram.generateHistogram(pairList, 10);
        List<String> expected = generateExpecteBasicStringList();

        assertEquals(5, result.size());
        assertEquals(expected.get(0), result.get(0));
        assertEquals(expected.get(1), result.get(1));
        assertEquals(expected.get(2), result.get(2));
        assertEquals(expected.get(3), result.get(3));
        assertEquals(expected.get(4), result.get(4));
    }

    @Test
    public void shouldResizeHistogram() {
        List<Pair<LocalDateTime, Integer>> pairList = generateshortPairList();
        List<String> result = Histogram.generateHistogram(pairList, 20);
        List<String> expected = generateResizedStringList();

        assertEquals(3, result.size());
        assertEquals(expected.get(0), result.get(0));
        assertEquals(expected.get(1), result.get(1));
        assertEquals(expected.get(2), result.get(2));
    }

    private List<String> generateResizedStringList() {
        List<String> stringList = new ArrayList<>();
        stringList.add("2018-11-05 01:00 | ==== 2");
        stringList.add("2018-11-05 02:00 | ========== 5");
        stringList.add("2018-11-05 03:00 | ==================== 10");
        return stringList;
    }

    private List<Pair<LocalDateTime, Integer>> generateshortPairList() {
        List<Pair<LocalDateTime, Integer>> pairList = new ArrayList<>();
        pairList.add(new Pair<>(LocalDateTime.of(2018, 11, 5, 1, 0), 2));
        pairList.add(new Pair<>(LocalDateTime.of(2018, 11, 5, 2, 0), 5));
        pairList.add(new Pair<>(LocalDateTime.of(2018, 11, 5, 3, 0), 10));
        return pairList;

    }

    private List<String> generateExpecteBasicStringList() {
        List<String> stringList = new ArrayList<>();
        stringList.add("2018-11-05 01:00 | == 2");
        stringList.add("2018-11-05 02:00 | ==== 4");
        stringList.add("2018-11-05 03:00 | ====== 6");
        stringList.add("2018-11-05 04:00 | ======== 8");
        stringList.add("2018-11-05 05:00 | ========== 10");
        return stringList;
    }

    private List<Pair<LocalDateTime, Integer>> generateBasicPairList() {
        List<Pair<LocalDateTime, Integer>> pairList = new ArrayList<>();
        pairList.add(new Pair<>(LocalDateTime.of(2018, 11, 5, 1, 0), 2));
        pairList.add(new Pair<>(LocalDateTime.of(2018, 11, 5, 2, 0), 4));
        pairList.add(new Pair<>(LocalDateTime.of(2018, 11, 5, 3, 0), 6));
        pairList.add(new Pair<>(LocalDateTime.of(2018, 11, 5, 4, 0), 8));
        pairList.add(new Pair<>(LocalDateTime.of(2018, 11, 5, 5, 0), 10));
        return pairList;
    }
}