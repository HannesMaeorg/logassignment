package helpers;

import model.Request;
import org.junit.Test;

import java.time.LocalDateTime;

import static org.junit.Assert.assertEquals;

public class RequestMapperTest {


    @Test
    public void shouldMapWithURI() {
        String logFileLine = "2015-08-19 00:00:09,967 (http--0.0.0.0-28080-3) [CUST:CUS12T2321] /mainContent.do?action=SUBSCRIPTION&msisdn=300445644148&contentId=main_subscription in 18";
        Request result = RequestMapper.map(logFileLine);
        Request expected = Request.builder()
                .time(LocalDateTime.of(2015, 8, 19, 0, 0, 9, 967000000))
                .threadId("(http--0.0.0.0-28080-3)")
                .userContext("[CUST:CUS12T2321]")
                .resource("/mainContent.do")
                .duration(18)
                .build();

        assertEquals(expected.getDuration(), result.getDuration());
        assertEquals(expected.getResource(), result.getResource());
        assertEquals(expected.getUserContext(), result.getUserContext());
        assertEquals(expected.getThreadId(), result.getThreadId());
        assertEquals(expected.getTime(), result.getTime());
    }

    @Test
    public void shouldMapWithResource() {
        String logFileLine = "2015-08-19 00:00:16,475 (http--0.0.0.0-28080-85) [] getPermission  300500274042 in 28";
        Request result = RequestMapper.map(logFileLine);
        Request expected = Request.builder()
                .time(LocalDateTime.of(2015, 8, 19, 0, 0, 16, 475000000))
                .threadId("(http--0.0.0.0-28080-85)")
                .userContext("[]")
                .resource("getPermission")
                .duration(28)
                .build();

        assertEquals(expected.getDuration(), result.getDuration());
        assertEquals(expected.getResource(), result.getResource());
        assertEquals(expected.getUserContext(), result.getUserContext());
        assertEquals(expected.getThreadId(), result.getThreadId());
        assertEquals(expected.getTime(), result.getTime());
    }
}