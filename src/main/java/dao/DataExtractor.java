package dao;

import helpers.RequestMapper;
import model.Request;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class DataExtractor {

    public List<Request> extract(String path) {
        List<Request> requests = new ArrayList<>();

        try {
            FileInputStream fstream = new FileInputStream(getFullFilePath(path));
            BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
            String strLine;
            while ((strLine = br.readLine()) != null) {
                requests.add(RequestMapper.map(strLine));
            }
            fstream.close();
        } catch (Exception e) {
            System.err.println("Error: " + e.getMessage());
        }
        return requests;
    }

    private String getFullFilePath(String path) throws URISyntaxException {
        final Class<?> referenceClass = DataExtractor.class;
        final URL url = referenceClass
                .getProtectionDomain()
                .getCodeSource()
                .getLocation();
        final File jarPath = new File(url.toURI()).getParentFile();
        return jarPath + "/" + path;
    }
}
