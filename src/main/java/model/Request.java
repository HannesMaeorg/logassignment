package model;

import lombok.Builder;
import lombok.Getter;
import java.time.LocalDateTime;

@Builder
@Getter
public class Request {

    private LocalDateTime time;
    private String threadId;
    private String userContext;
    private String resource;
    private long duration;
}


